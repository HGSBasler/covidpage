from app import app, register_callbacks, main_layout


register_callbacks()

app.layout = main_layout


if __name__ == "__main__":
    app.run_server(debug=False)
