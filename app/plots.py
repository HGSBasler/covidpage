import re
import datetime as dt
from typing import Tuple, Optional

import pandas as pd
import numpy as np
from plotly import graph_objects as go
from plotly.subplots import make_subplots

ICU_URL = "https://raw.githubusercontent.com/openZH/covid_19/master/fallzahlen_kanton_zh/COVID19_Belegung_Intensivpflege.csv"


def make_weekly_plot(zh: pd.DataFrame) -> Tuple[go.Figure, str]:

    zh['date'] = pd.to_datetime(zh["date"])
    zh["Weekday"] = zh.date.dt.weekday
    zh["Diff"] = zh["ncumul_conf"].diff()

    weekday_names = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
    weekday_long = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    weekday = zh.iloc[-1].Weekday
    zh_weekly = zh.groupby(pd.Grouper(key="date", freq=f"W-{weekday_names[weekday]}")).sum()

    updated = np.datetime_as_string(max(zh_weekly.index.values), unit="D")

    weekly_graph = go.Figure()

    weekly_graph.add_trace(
        go.Bar(
            x=zh_weekly.index,
            y=zh_weekly.Diff,
        )
    )

    weekly_graph.update_layout(
        template="plotly_dark",
        title=f"Sum of new cases in seven day periods ending {weekday_long[weekday]}"
    )

    return weekly_graph, f"Last update: {updated}"


def make_14_day_incidence(zh: pd.DataFrame, days: int = 21) -> Tuple[go.Figure, int, int, int]:

    zh['date'] = pd.to_datetime(zh["date"])
    zh["Weekday"] = zh.date.dt.weekday
    zh["Diff"] = zh["ncumul_conf"].diff()

    population = 1_537_518

    df = (zh.set_index("date").Diff / population * 100_000).rolling(14).sum()
    df7 = (zh.set_index("date").Diff / population * 100_000).rolling(7).sum()

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig.add_trace(
        go.Scatter(
            x=df.index,
            y=df,
            name="14 days (right)",
            mode="lines",
            line=dict(width=3),
        ),
        secondary_y=True,
    )

    fig.add_trace(
        go.Scatter(
            x=df7.index,
            y=df7,
            name="7 days (right)",
            mode="lines",
            line=dict(width=3),
        ),
        secondary_y=True,
    )

    fig.add_trace(
        go.Bar(
            x=zh["date"],
            y=zh["Diff"] / population * 100_000,
            name="per day (left)",
        ),
        secondary_y=False,
    )

    fig.update_layout(
        template="plotly_dark",
        title="per 100k inhabitants",
        legend=dict(orientation="h"),
        xaxis=dict(
            rangeslider=dict(
                visible=True,
            ),
            range=(zh.date.iloc[-days], zh.date.iloc[-1] + dt.timedelta(days=1)),
        ),
    )

    return fig, np.round(df.iloc[-1], 3), np.round(df7.iloc[-1], 3), population


def make_daily(zh: pd.DataFrame, days: int = 21):
    zh["date"] = pd.to_datetime(zh["date"])
    zh["Diff"] = zh.ncumul_conf.diff()

    fig = go.Figure()

    fig.add_trace(
        go.Bar(
            x=zh.date,
            y=zh.Diff,
        )
    )
    fig.update_layout(
        template="plotly_dark",
        xaxis=dict(
            rangeslider=dict(
                visible=True,
            ),
            range=(zh.date.iloc[-days], zh.date.iloc[-1] + dt.timedelta(days=1)),
        ),
    )

    return (fig, )


def make_plz(df: pd.DataFrame, plz: int) -> Tuple[go.Figure]:

    def extract_min(x: str) -> Optional[int]:
        m = re.search(r"(\d{1,})", x)
        if m:
            return int(m.group())
        return None

    def extract_diff(x: str) -> Optional[int]:
        m = re.findall(r"(\d{1,})", x)
        if len(m) == 2:
            return int(m[1]) - int(m[0])
        return None

    df["Date"] = pd.to_datetime(df.Date)
    df["Weekday"] = df.Date.dt.weekday
    df = df.set_index("Date").sort_index()

    df = df[df.Weekday == df.Weekday.iloc[-1]]
    df = df[df.PLZ == str(plz)]
    df["min"] = df.NewConfCases_7days.apply(extract_min)
    df["diff"] = df.NewConfCases_7days.apply(extract_diff)

    fig = go.Figure()

    fig.add_trace(
        go.Bar(
            x=df.index,
            y=df["min"],
            marker=dict(color="#5555FA"),
            showlegend=False,
        )
    )

    fig.add_trace(
        go.Bar(
            x=df.index,
            y=df["diff"],
            marker=dict(color="#AAAAFA"),
            showlegend=False,
        )
    )

    fig.update_layout(
        barmode="stack",
        template="plotly_dark",
        title=f"Postal Code: {plz}",
    )

    return (fig, )


def make_icu_plot(hospital: Optional[str] = None) -> go.Figure:
    df = pd.read_csv(ICU_URL)
    if hospital:
        df = df[df.hospital_name == hospital]

    # only ZH
    df = df[df.abbreviation_canton_and_fl == "ZH"]

    df = df.drop(["time", "abbreviation_canton_and_fl", "hospital_name", "source"], axis=1).groupby("date").sum()

    free = df.current_icu_service_certified - df.current_icu_covid - df.current_icu_not_covid
    free[free < 0] = 0

    fig = go.Figure()

    fig.add_traces([
        go.Bar(
            x=df.index,
            y=df.current_icu_covid,
            name="Covid",
        ),
        go.Bar(
            x=df.index,
            y=df.current_icu_not_covid,
            name="Not Covid",
        ),
        go.Bar(
            x=df.index,
            y=free,
            name="Free",
        ),
    ])

    fig.update_layout(
        barmode="stack",
        title="ICU capacity - " + (hospital if hospital else "All hospitals in the canton ZH"),
        legend=dict(orientation="h", y=1.1),
        template="plotly_dark",
    )

    return fig
