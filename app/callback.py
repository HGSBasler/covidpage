import dash
import json
import pandas as pd
from typing import Optional

import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, ALL
from dash.exceptions import PreventUpdate
from . import app
from .plots import make_weekly_plot, make_14_day_incidence, make_daily, make_plz, make_icu_plot

url_canton = "https://raw.githubusercontent.com/openZH/covid_19/master/fallzahlen_kanton_zh/\
COVID19_Fallzahlen_Kanton_ZH_total.csv"
ICU_URL = "https://raw.githubusercontent.com/openZH/covid_19/master/fallzahlen_kanton_zh/COVID19_Belegung_Intensivpflege.csv"


def register_callbacks():
    @app.callback(
        [
            Output("graph1", "figure"), Output("updated", "children"),
            Output("graph2", "figure"), Output("incidence", "children"), Output("highest_value", "children"),
            Output("population", "children"),
            Output("graph3", "figure"),
            Output("hospital_dropdown", "children"),
        ],
        [Input("location", "path"), ]
    )
    def on_load(path):
        zh = pd.read_csv(url_canton)
        days = 8 * 7
        df = pd.read_csv(ICU_URL)
        hosp_dropdown = [
            dbc.DropdownMenuItem("All hospitals", id={"type": 'hospital', "id": "all"}),
            dbc.DropdownMenuItem(divider=True),
        ]
        hosp_dropdown += [dbc.DropdownMenuItem(h, id={"type": 'hospital', "id": h}) for h in df.hospital_name.unique()]
        return (
            make_weekly_plot(zh) + make_14_day_incidence(zh, days=days) + make_daily(zh, days=days) + (hosp_dropdown,)
        )

    @app.callback(
        Output("graph4", "figure"),
        [Input("store_plz", "data"), ]
    )
    def plz_graph(plz):
        df = pd.read_csv("https://raw.githubusercontent.com/openZH/covid_19/master/fallzahlen_plz/fallzahlen_kanton_ZH_plz.csv")
        plz = plz if plz else 8001
        return make_plz(df, plz)[0]

    @app.callback(
        Output("store_plz", "data"),
        [Input({"type": "plz_dropdown", "id": ALL}, "n_clicks")]
    )
    def change_plz_dropdown(value):
        ctx = dash.callback_context
        if not ctx.triggered:
            raise PreventUpdate

        return json.loads(ctx.triggered[0]['prop_id'].split('.')[0])["id"]

    @app.callback(
        Output("store_hospital", "data"),
        [Input({"type": "hospital", "id": ALL}, "n_clicks")]
    )
    def change_hospital_dropdown(value):
        ctx = dash.callback_context
        if not ctx.triggered:
            raise PreventUpdate

        hospital = json.loads(ctx.triggered[0]["prop_id"].split(".")[0])["id"]
        return None if hospital == "all" else hospital

    @app.callback(
        Output("graph5", "figure"),
        [Input("store_hospital", "data")]
    )
    def change_icu_plot(hospital: Optional[str]):
        return make_icu_plot(hospital)
