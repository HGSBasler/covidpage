import json
import urllib
from typing import List

import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html


PLZ_URL = "https://raw.githubusercontent.com/openZH/covid_19/master/fallzahlen_plz/PLZ_gen_epsg2056_F_KTZH_2020.json"


def make_plz_options() -> List[dbc.DropdownMenuItem]:
    txt = urllib.request.urlopen(PLZ_URL).read()
    features = json.loads(txt)["features"]
    plz = {
        str(int(f["properties"]["PLZ"])): f["properties"]["Ortschaftsname"] for f in features if f["properties"]["PLZ"]
    }
    return [
        dbc.DropdownMenuItem(f"{p}: {plz[p]}", id={"type": "plz_dropdown", "id": int(p)}) for p in sorted(plz)
    ]


tab_weekly = dbc.Card(
    dbc.CardBody(
        dbc.Spinner([
            dbc.Row(dbc.Col(html.H3("Seven day periods"))),
            dbc.Row(dbc.Col(
                dcc.Graph(
                    id="graph1",
                    config=dict(displayModeBar=False,),
                ), width=12)),
        ]),
    )
)

tab_incidence = dbc.Card(
    dbc.CardBody(
        dbc.Spinner([
            dbc.Row(dbc.Col(html.H3("Incidence"))),
            dbc.Row(dbc.Col(
                dcc.Graph(
                    id="graph2",
                    config=dict(displayModeBar=False,),
                ), width=12)),
            dbc.Row([
                dbc.Col(dbc.Card(
                    dbc.CardBody([
                        html.H4("Current Value (14)", className="card-title"),
                        html.H2(id="incidence")
                    ]),
                )),
                dbc.Col(dbc.Card(
                    dbc.CardBody([
                        html.H4("Current Value (7)", className="card-title"),
                        html.H2(id="highest_value"),
                    ])
                )),
                dbc.Col(dbc.Card(
                    dbc.CardBody([
                        html.H4("Population", className="card-title"),
                        html.H2(id="population"),
                    ])
                ))
            ]),
        ]),
    )
)


tab_daily = dbc.Card(
    dbc.CardBody(
        dbc.Spinner([
            dbc.Row(dbc.Col(html.H3("New cases by day"))),
            dbc.Row(dbc.Col(
                dcc.Graph(
                    id="graph3",
                    config=dict(displayModeBar=False,),
                ), width=12)),
        ]),
    )
)


tab_plz = dbc.Card(
    dbc.CardBody(
        dbc.Spinner([
            dbc.Row(dbc.Col(html.H3("Cases by postal code; last 7 days (range)"))),
            dbc.Row(dbc.Col(dbc.DropdownMenu(make_plz_options(), label="Select postal code"))),
            dbc.Row(dbc.Col(
                dcc.Graph(
                    id="graph4",
                    config=dict(displayModeBar=False,),
                ), width=12)),
        ]),
    )
)

tab_icu = dbc.Card(
    dbc.CardBody(
        dbc.Spinner([
            dbc.Row(dbc.Col(html.H3("Intensive Care Unit capacity"))),
            dbc.Row(dbc.Col(dbc.DropdownMenu(id="hospital_dropdown", label="Select Hopsital"))),
            dbc.Row(dbc.Col(
                dcc.Graph(
                    id="graph5",
                    config=dict(displayModeBar=False,),
                ), width=12),
            )
        ])
    )
)


main_layout = dbc.Container(
    [
        dcc.Location(id="location"),
        dcc.Store(id="store_plz", storage_type="local"),
        dcc.Store(id="store_hospital", storage_type="local"),
        dbc.Row(dbc.Col(html.H1("Covid-19 Zürich"))),
        dbc.Row(dbc.Col(dbc.Label(id="updated"))),
        dbc.Tabs(
            [
                dbc.Tab(tab_daily, label="Daily"),
                dbc.Tab(tab_weekly, label="7 days",),
                dbc.Tab(tab_incidence, label="Incidence"),
                dbc.Tab(tab_plz, label="by postal code"),
                dbc.Tab(tab_icu, label="ICU"),
            ]
        ),
        dbc.Row(dbc.Col(dbc.Label(["Data from ", html.A("OpenZH", href="https://github.com/openZH/covid_19")]))),
    ],
    className="p-4",
)
